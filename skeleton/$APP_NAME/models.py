from django.db import models

from channels_redux.notify import NotifierMixin


class ExampleModel(models.Model, NotifierMixin):
    example_field = models.CharField(max_length=50)
