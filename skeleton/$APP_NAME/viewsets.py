from rest_framework import viewsets

from $APP_NAME.models import ExampleModel
from $APP_NAME.serializers import ExampleModelSerializer


class ExampleModelViewSet(viewsets.ModelViewSet):
    queryset = ExampleModel.objects.all()
    serializer_class = ExampleModelSerializer
