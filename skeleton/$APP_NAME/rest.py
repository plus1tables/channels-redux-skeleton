from channels_redux.rest import RestRouter

from $APP_NAME import viewsets

router = RestRouter()
router.register(r'$APP_NAME/example-models', viewsets.ExampleModelViewSet)
