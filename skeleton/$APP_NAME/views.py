from django.contrib.auth import views as auth_views

from django_react_views import views


class HomeView(views.ReactTemplateView):
    react_component = 'HomePage.js'
    template_name = "$APP_NAME/home.html"


class LoginView(auth_views.LoginView):
    template_name = '$APP_NAME/login.html'

