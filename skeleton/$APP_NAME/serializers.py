from channels_redux.rest import HyperlinkedModelSerializer

from $APP_NAME.models import ExampleModel


class ExampleModelSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = ExampleModel
        fields = ('url', 'pk', 'example_field')
