import {createStore, WebSocketHandler} from "channels-redux"
import { Provider } from 'react-redux'
import React from 'react'
import { render } from 'react-dom'
import HomeComponent from "./components/HomeComponent"

const store = createStore();

const webSocketHandler = new WebSocketHandler(store);
webSocketHandler.subscribeAll();

render(
    <Provider store={store}>
        <HomeComponent webSocketHandler={webSocketHandler}/>
    </Provider>,
    window.react_mount
);
