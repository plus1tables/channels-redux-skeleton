import React from 'react';


class HomeComponent extends React.Component{
    constructor(props) {
        super(props);
        this.state = {socketFailing: false};

        this.checkSocket = this.checkSocket.bind(this);

        this.webSocketHandler = props.webSocketHandler;
        setTimeout(this.checkSocket, 1000);
    }

    checkSocket() {
        if (this.webSocketHandler.connectionFailureCount > 0) {
            this.setState({socketFailing: true});
            return
        } else if (this.webSocketHandler.webSocket.readyState === WebSocket.OPEN)  {
            return
        }
        setTimeout(this.checkSocket, 1000);
    }

    render() {
        return <React.Fragment>
            <h1 className={""}>Hello World</h1>
            {this.state.socketFailing ?
                <span className={"alert alert-danger"}>
                    WebSocket connection failing. Are you logged in? <a href={"/admin/"}>Log In Here</a>
                </span>
                : null}
        </React.Fragment>
    }
}

export default HomeComponent;
