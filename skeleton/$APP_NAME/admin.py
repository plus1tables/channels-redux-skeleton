from django.contrib import admin

# Register your models here.
from $APP_NAME.models import ExampleModel

admin.site.register(ExampleModel)