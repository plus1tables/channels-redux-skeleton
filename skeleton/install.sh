#!/usr/bin/env bash

open_browser () {
    if which xdg-open > /dev/null
    then
      xdg-open $1
    elif which gnome-open > /dev/null
    then
      gnome-open $1
    fi
}

npm install
npm run build

docker run --name=redis -p 6379:6379 -d redis:4.0
docker start redis

virtualenv venv -p python3
source venv/bin/activate
pip install -r requirements.txt

python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser

(sleep 2 ; open_browser 'http://localhost:8008/login/') &
python manage.py runserver 0.0.0.0:8008
