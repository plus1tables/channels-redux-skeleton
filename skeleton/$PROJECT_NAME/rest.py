from channels_redux.rest import RestRouter
from $APP_NAME.rest import router as $APP_NAME_router

router = RestRouter()
router.registry.extend($APP_NAME_router.registry)
