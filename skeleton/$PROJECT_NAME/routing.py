from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter

from channels_redux.notify import NotifyRouter

application = ProtocolTypeRouter({
    "websocket": AuthMiddlewareStack(
        NotifyRouter()
    )
})
