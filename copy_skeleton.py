#!/usr/bin/env python3

import os
import os.path
import shutil

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
SKELETON_DIR = os.path.join(BASE_DIR, 'skeleton')

DEFAULT_PROJECT_NAME = "example_proj"
DEFAULT_PROJECT_DIR = DEFAULT_PROJECT_NAME
DEFAULT_APP_NAME = "example"


def _get_project_root():
    return input("Project Parent Directory (default='.'):") or '.'


def startproj():
    project_root = _get_project_root()
    project_name = input("Project Name ({}):".format(project_root)) or project_root
    copyproj(project_root, project_name)
    replace_in_dir(project_root, "$PROJECT_NAME", project_name)
    return project_root


def startapp(project_root):
    if project_root is None:
        project_root = _get_project_root()
    app_name = input("App Name:")
    copyapp(project_root, app_name)


def copyapp(target_dir, name):
    dest = os.path.join(target_dir, name)
    replace_in_dir(target_dir, "class $APP_NAME", "class {}".format(name.capitalize()))
    shutil.copytree(os.path.join(SKELETON_DIR, "$APP_NAME"), dest)
    replace_in_dir(os.path.abspath(os.path.join(dest, '..')), "$APP_NAME", name)


def copyproj(target_dir, name):
    skip_files = {'node_modules', '$APP_NAME'}

    def ignore_func(dir_name, children):
        return skip_files
    shutil.copytree(SKELETON_DIR, target_dir, ignore=ignore_func)
    replace_in_dir(target_dir, "class $PROJECT_NAME", "class {}".format(name.capitalize()))
    replace_in_dir(target_dir, "$PROJECT_NAME", name)


def replace_in_dir(project_root, orig, new):
    to_move = []

    for root, dirs, files in os.walk(project_root):
        for dirname in dirs:
            if orig in dirname:
                to_move.append(os.path.join(root, dirname))

        for fname in files:
            if orig in fname:
                to_move.append(os.path.join(root, fname))

            fpath = os.path.join(root, fname)
            with open(fpath) as f:
                s = f.read()
            s = s.replace(orig, new)
            with open(fpath, "w") as f:
                f.write(s)

    for fullpath in to_move:
        shutil.move(fullpath, fullpath.replace(orig, new))


def startdefault():
    shutil.rmtree(DEFAULT_PROJECT_DIR)
    copyproj(DEFAULT_PROJECT_DIR, DEFAULT_PROJECT_NAME)
    copyapp(DEFAULT_PROJECT_DIR, DEFAULT_APP_NAME)


def _main():
    import sys
    project_root = None
    if 'startdefault' in sys.argv:
        startdefault()
        return
    if 'startproj' in sys.argv or 'startall' in sys.argv:
        project_root = startproj()
    if 'startapp' in sys.argv or 'startall' in sys.argv:
        startapp(project_root)


if __name__ == '__main__':
    _main()
