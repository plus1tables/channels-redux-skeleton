# channels-redux-skeleton

## Usage
Use copy_skeleton to create a new project

```
./copy_skeleton.py startall
```

You will be prompted for a project directory, a project name, and an app name.

After this finished copying over cd into the project directory

Run the helper script
```
./install.sh
```

When this finishes you're browser should open to the login screen for your new app